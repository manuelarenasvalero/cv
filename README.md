
</br>

<img align="center" src="store/images/banner.png"/>

</br>

# Resume

[TOC]

</br></br>

# Presentation

<a>
<h3 align="center" style="font-family: Verdana">Hello! My name is Manuel.</h3>
</a>

<a>
<h3 align="center" style="font-family: Verdana">
I'm a QA software engineer focused on {+ automation+} tests into {+ agile+}  development life cycle, specially on functional tests over APIs and acceptance test level.
</a></h3>

<a>
<h3 align="center" style="font-family: Verdana">
I have been performing a QA role {- since 2017-}  and started as a DEV for 2+ years.</h3>
</a>

</br></br>

# My tech stack

<table border="0">
<tbody>
<tr><td style="width: 33%;">

<!-- BACKEND SKILLS -->

<img src="https://img.shields.io/badge/Backend-KARATE-white?labelColor=green&style=plastic"/>
<img src="https://img.shields.io/badge/Backend-FITNESSE-white?labelColor=green&style=plastic"/>
<img src="https://img.shields.io/badge/Backend-POSTMAN-white?labelColor=green&style=plastic"/>
<img src="https://img.shields.io/badge/Backend-SOAPUI-white?labelColor=green&style=plastic"/>
<img src="https://img.shields.io/badge/Backend-WIREMOCK-white?labelColor=green&style=plastic"/>
<img src="https://img.shields.io/badge/Backend-KAFKA-white?labelColor=green&style=plastic"/>
<img src="https://img.shields.io/badge/Backend-API%20REST-white?labelColor=green&style=plastic"/>
<img src="https://img.shields.io/badge/Backend-JSON-white?labelColor=green&style=plastic"/>
<img src="https://img.shields.io/badge/Backend-MAVEN-white?labelColor=green&style=plastic"/>

</td><td style="width: 33%;">

<!-- FRONTEND SKILLS -->

<img src="https://img.shields.io/badge/Frontend-SELENIUM-white?labelColor=blue&style=plastic"/>
<img src="https://img.shields.io/badge/Frontend-TESTCOMPLETE-white?labelColor=blue&style=plastic"/>
<img src="https://img.shields.io/badge/Frontend-CYPRESS-white?labelColor=blue&style=plastic"/>
<img src="https://img.shields.io/badge/Frontend-PLAYWRIGHT-white?labelColor=blue&style=plastic"/>

</td><td style="width: 33%;">

<!-- PERFORMANCE SKILLS -->

<img src="https://img.shields.io/badge/Performance-JMETER-white?labelColor=yellow&style=plastic"/>
<img src="https://img.shields.io/badge/Performance-GATLING-white?labelColor=yellow&style=plastic"/>
<img src="https://img.shields.io/badge/Performance-LIGHTHOUSE-white?labelColor=yellow&style=plastic"/>

</td></tr>
<tr><td>

<!-- LANGUAGES SKILLS -->

<img src="https://img.shields.io/badge/Language-JAVASCRIPT-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-GHERKIN-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-SELENESE-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-JAVA-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-C++-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-C--SHARP-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-SHELL-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-HTML-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-XML-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-UML-white?labelColor=grey&style=plastic"/>
<img src="https://img.shields.io/badge/Language-COBOL-white?labelColor=grey&style=plastic"/>

</td><td>

<!-- PIPELINES SKILLS -->

<img src="https://img.shields.io/badge/Pipeline-JENKINS-white?labelColor=orange&style=plastic"/>
<img src="https://img.shields.io/badge/Pipeline-SONARQUBE-white?labelColor=orange&style=plastic"/>
<img src="https://img.shields.io/badge/Pipeline-DOCKER-white?labelColor=orange&style=plastic"/>
<img src="https://img.shields.io/badge/Pipeline-NEXUS-white?labelColor=orange&style=plastic"/>
<img src="https://img.shields.io/badge/Pipeline-KIUWAN-white?labelColor=orange&style=plastic"/>

</td><td>

<!-- TCMS SKILLS -->

<img src="https://img.shields.io/badge/TCMS-XRAY-white?labelColor=indigo&style=plastic"/>
<img src="https://img.shields.io/badge/TCMS-HPQC-white?labelColor=indigo&style=plastic"/>
<img src="https://img.shields.io/badge/TCMS-TESTLINK-white?labelColor=indigo&style=plastic"/>
<img src="https://img.shields.io/badge/TCMS-SPIRATEST-white?labelColor=indigo&style=plastic"/>
<img src="https://img.shields.io/badge/TCMS-AZURE%20TEST%20PLANS-white?labelColor=indigo&style=plastic"/>

</td></tr>
<tr><td>

<!-- IDE SKILLS -->


<img src="https://img.shields.io/badge/IDE-VISUAL%20STUDIO%20CODE-white?labelColor=magenta&style=plastic"/>
<img src="https://img.shields.io/badge/IDE-KATALON%20STUDIO-white?labelColor=magenta&style=plastic"/>
<img src="https://img.shields.io/badge/IDE-INTELIJ-white?labelColor=magenta&style=plastic"/>
<img src="https://img.shields.io/badge/IDE-FIGMA-white?labelColor=magenta&style=plastic"/>

</td><td>

<!-- VCS SKILLS -->

<img src="https://img.shields.io/badge/VCS-GIT-white?labelColor=red&style=plastic"/>
<img src="https://img.shields.io/badge/VCS-SVN-white?labelColor=red&style=plastic"/>

</td><td>

<!-- DB SKILLS -->

<img src="https://img.shields.io/badge/DB-MYSQL-white?labelColor=teal&style=plastic"/>
<img src="https://img.shields.io/badge/DB-SQL%20SERVER-white?labelColor=teal&style=plastic"/>
<img src="https://img.shields.io/badge/DB-DB2-white?labelColor=teal&style=plastic"/>

</td></tr>
</tbody>
</table>
<hr>

</br></br>

# My certifications

<h4 style="font-family: Verdana">I have some achieved certifications that you may found interesting.</h4>

<table>
  <tr>
    <td width="1"></td>
    <td colspan="2" align="center"><img src="https://img.shields.io/badge/ISTQB%20CTFL-red?style=for-the-badge&logo=openbugbounty&logoColor=white" title="Certified Tester Foundation Level" alt="ISTQB CTFL"/></td>
    <td width="1"></td>
  </tr>
  <tr>
    <td colspan="2" align="center" width="1"><img src="https://img.shields.io/badge/ITIS-orange?style=for-the-badge&logo=Informatica&logoColor=white" title="Technical Engineering in Computer Systems" alt="ITIS"/></td>
    <td colspan="2" align="center"><img src="https://img.shields.io/badge/Docker-2496ED?style=for-the-badge&logo=Docker&logoColor=white" title="Docker Basic Certification" alt="DOCKER"/></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2" align="center"><img src="https://img.shields.io/badge/Scrum%20SFPC-6A5ACD?style=for-the-badge&logo=scrumalliance&logoColor=white" title="Scrum Foudation Professional Certificate" alt="SCRUM SFPC"/></td>
    <td></td>
  </tr>
</table>


</br></br>

# Automation samples

<h4 style="font-family: Verdana">See some samples about POC automations on my portfolio.</h4>

<a href="https://gitlab.com/users/manuelarenasvalero/groups" target="_blank">
<img src=https://img.shields.io/badge/PORTFOLIO-3CB371?&style=for-the-badge&logo=pinboard&logoColor=white title="POC proyects on GitLab" alt="POC proyects on GitLab" style="margin-bottom: 5px;" />
</a>

</br></br>

# My project perfect match
<h4 style="font-family: Verdana">Sneak peek of my ideal project to fit me in.</h4>

<img src="store/images/MoSCoW_Matrix_for_project.png"/>

</br></br>

# Preview and download my CV in Europass format (PDF)

<a href="../raw/main/store/CV_-_SPA.pdf?inline=true" target="_blank">
<img src=https://img.shields.io/badge/CV%20(spanish)-brigthgreen?style=for-the-badge&logoColor=white title="CV in Spanish" alt="CV - Spanish" style="margin-bottom: 5px;" />
</a>  
<a href="../raw/main/store/CV_-_ENG.pdf?inline=true" target="_blank">
<img src=https://img.shields.io/badge/CV%20(english)-important?style=for-the-badge&logoColor=white title="CV in English" alt="CV - English" target="_blank" style="margin-bottom: 5px;" />
</a> 

</br></br>

# Connect with me  

<a href="https://www.linkedin.com/in/manuel-a-5680b7a1/" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5?&style=for-the-badge&logo=linkedin&logoColor=white title="Check my LinkedIn profile" alt="Linkedin profile" style="margin-bottom: 5px;" />
</a>
<a href="mailto:manuelarenasvalero@gmail.com" target="_blank">
<img src=https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white title="Compose me an email" alt="Gmail email" style="margin-bottom: 5px;" />
</a>  
 
<hr>